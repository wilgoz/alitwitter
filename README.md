# AliTwitter
Features
- Create tweets
- Read tweets
- Delete tweets

## Environment Setup
- Ruby 2.6.5
- Bundler 2.1.4
- Rails 6.0.2.1

install all dependency using bundle in root of directory project.
```
bundle install
```

## Run with Docker
```
docker build -t alitwtiter .
docker run -itd --name alitwitter \
    -p 3000:3000 \
    -e DATABASE_HOST: '{{ psql.host }}' \
    -e DATABASE_NAME: '{{ psql.database }}' \
    -e DATABASE_USER: '{{ psql.user }}' \
    -e DATABASE_PASS: '{{ psql.password }}' \
    alitwitter
```
Access through localhost:3000

## Run with Vagrant
### Create the build artifact
```
docker build -t alitwtiter .
docker save alitwitter -o provision/roles/load/files/alitwitter.tar
```

### Deploy the artifact within a Vagrant box
Set up the virtual machines
```
vagrant up
```
Execute the deploy ansible script
```
ansible-playbook -i provision/hosts/hosts.yml provision/deploy.yml
```
Access through localhost:3000

### Redeploy the artifact
```
ansible-playbook -i provision/hosts/hosts.yml provision/redeploy.yml
```
